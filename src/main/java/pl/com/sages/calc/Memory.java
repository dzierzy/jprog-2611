package pl.com.sages.calc;

public interface Memory {

    double readValue();

    void writeValue(double value);

    int capacity();

}
