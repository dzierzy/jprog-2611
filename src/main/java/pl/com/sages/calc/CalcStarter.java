package pl.com.sages.calc;

import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class CalcStarter {

    public static void main(String[] args) {
        System.out.println("Let's count something :)");

        try {
            ScienceCalculator c = new ScienceCalculator(2.0);
            c.load();

            c.add(2);
            c.divide(3);
            c.multiply(2.21);
            c.add(34.78);
            c.divide(12.12);

            c.pow(7);

            c.multiply(2);

            Locale locale = Locale.CANADA_FRENCH;

            NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
            nf.setMaximumFractionDigits(5);
            nf.setMinimumFractionDigits(2);

            LocalDateTime date = LocalDateTime.now();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("YYYY~MM~dd");
            String dateString =  dtf.format(date);

            System.out.println("[" + dateString + "] result=" + nf.format(c.getValue()));
            System.out.println("c= " + c);

            c.save();


        }catch (Exception e){
            System.out.println("unhandled method occured. message: " + e.getMessage());
            e.printStackTrace();
            System.exit( -1 );
        }

    }

}
