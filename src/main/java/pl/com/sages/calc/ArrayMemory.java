package pl.com.sages.calc;

import java.util.Arrays;

@MultiValue(count=5)
public class ArrayMemory implements Memory {


    private double[] values = new double[5];

    private int index = 0;

    @Override
    public double readValue() {
        return values[index];
    }

    @Override
    public void writeValue(double value) {
        if (index < values.length-1) {
            values[++index] = value;
        } else {
            // [0, 1, 2, 3, 4] (4) + 5
            // [1, 2, 3, 4, 5] (4) !
            // [5, 1, 2, 3, 4] (0)
            System.arraycopy(values, 1, values, 0, values.length - 1); // [1,2,3,4,4]
            values[index] = value;
        }
    }

    @Override
    public int capacity() {
        return values.length;
    }

    @Override
    public String toString() {
        return "ArrayMemory{" +
                "values=" + Arrays.toString(values) +
                ", index=" + index +
                '}';
    }
}
