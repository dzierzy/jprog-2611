package pl.com.sages.calc;

import java.sql.*;
import java.util.List;

public class DBConnection {


    void save(List<Double> values){
        // TODO implement data saving

        try(Connection c = getConnection();){
            c.setAutoCommit(false);

            try {
                PreparedStatement stmt = c.prepareStatement("insert into calculator(value) values (?)");

                int count = 0;
                for (double value : values) {
                    count++;
                    if(count>15){
                        throw new SQLException("fake");
                    }
                    stmt.setDouble(1, value);
                    stmt.addBatch();
                    //int amount = stmt.executeUpdate();
                    //System.out.println(amount + " row(s) affected.");
                }
                stmt.executeBatch();

                c.commit();

            }catch (SQLException sqle){
                c.rollback();
                throw sqle;
            }

        } catch (SQLException e) {
            e.printStackTrace();

        }

        System.out.println("saved and disconnected.");
    }

    double load(){
        // TODO implement data loading

        double result = -1;

        try(Connection c = getConnection()){
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("select value from calculator order by id desc limit 1");

            if(rs.next()){
                result = rs.getDouble("value");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    private Connection getConnection(){

        String driverName = "com.mysql.cj.jdbc.Driver";
        String url = "jdbc:mysql://localhost/calculator?serverTimezone=UTC";
        String user = "root";
        String password = "mysql";

        try {
            Class.forName(driverName);
            Connection c = DriverManager.getConnection(url, user, password);
            System.out.println("connected.");
            return c;
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

}
