package pl.com.sages.calc;

import java.io.IOException;

/**
 * extended version of a calculator adding sceintific operations...
 *
 * @author marcin
 * @since 1.1
 * @see Calculator
 */
public class ScienceCalculator extends Calculator {


    public ScienceCalculator(double value){
        super( value );
    }

    /**
     * constructing an object using default 0.0
     */
    public ScienceCalculator(){
        super();
    }


    /**
     * counts power of current value. accepts positive operand only
     * @param operand power operand
     * @return result of a calculation
     * @throws IllegalArgumentException in case of negative operand
     * @since 1.2
     */
    public double pow(int operand) throws CalculatorException, IOException {
/*
        if (operand == 0) {
            //return value = 1;
            throw new IOException("fake exception");
        } else if(operand < 0){
            throw new CalculatorException("can not count power for negative number");
        }*/

         memory.writeValue(Math.pow(memory.readValue(), operand));
         return memory.readValue();

        /*double val = value;

        for (int i=1; i<operand; i++) {
            value *= val;
        }

        return value;*/
    }



}
