package pl.com.sages.calc;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Calculator {



    protected Memory memory;

    public Calculator(double value){
        initMemory();
        System.out.println("installling memory of capacity " + memory.capacity());
        memory.writeValue(value);
    }

    public Calculator(){
        this(0.0);
    }

    public void save(){
        DBConnection dbc = new DBConnection();
        if(memory instanceof ListMemory){
            ListMemory lm = (ListMemory) memory;
            dbc.save(lm.getValues());
        } else {
            List<Double> values = new ArrayList<>();
            values.add(memory.readValue());
            dbc.save(values);
        }
    }

    public void load(){
        DBConnection dbc = new DBConnection();
        double loadedValue = dbc.load();
        memory.writeValue(loadedValue);
    }


    private void initMemory(){

        File configFile = new File("src/main/resources/memory.config");
        String memoryClassName = null;
        try(BufferedReader br = new BufferedReader(new FileReader(configFile));){

            memoryClassName = br.readLine();
            memoryClassName = memoryClassName.trim();
            System.out.println("memory class name = [" + memoryClassName + "]");

        } catch (IOException e) {
            e.printStackTrace();
        }

       /* try(BufferedWriter bw = new BufferedWriter(new FileWriter("backup.config", false))){
            bw.write(memoryClassName + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        try {
            Class clazz = Class.forName(memoryClassName);
            Constructor constructor = clazz.getConstructor();
            Object o = constructor.newInstance();
            if(o instanceof Memory) {
                memory = (Memory) o;
                System.out.println("installing configured memory implementation: " + memory.getClass().getName());

                MultiValue mv = memory.getClass().getAnnotation(MultiValue.class);
                if(mv!=null){
                    System.out.println("provided memory is multivalued of amount " + mv.count());
                }

                for(Field f : memory.getClass().getDeclaredFields()){
                    System.out.println("field " + f.getName() + ", " + f.getType().getName());
                }

                for(Method m : memory.getClass().getMethods()){
                    System.out.println(
                            "method " + m.getName() +
                                    ", " + m.getReturnType().getName() +
                                    ", " + Arrays.toString(m.getParameterTypes()));
                }

                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        System.out.println("installing simple memory implementation");
        memory = new SimpleMemory();
    }


    public double add(double operand){
        memory.writeValue(memory.readValue()+operand);
        return memory.readValue();
    }

    private double subtract(double operand){
        memory.writeValue(memory.readValue()-operand);
        return memory.readValue();
    }

    public double multiply(double operand){
        memory.writeValue(memory.readValue()*operand);
        return memory.readValue();
    }

    /**
     *
     * @param operand
     * @return
     * @deprecated
     */
    @Deprecated
    public double divide(double operand){
        memory.writeValue(memory.readValue()/operand);
        return memory.readValue();
    }

    public double getValue() {
        return memory.readValue();
    }



    public static int multiply(int a, int b){
        return a*b;
    }


    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Calculator{");
        sb.append("memory=").append(memory);
        sb.append('}');
        return sb.toString();
    }
}
