package pl.com.sages.charger;


public class ChargingStarter {

    public static void main(String[] args) {

        System.setOut(new ThreadPrintStream());



        Charger samsungCharger = new Charger();
        Charger iphoneCharger = new Charger();

        Phone iphone = new Phone(50, "iphone8");
        Phone samsung = new Phone(75, "samsung s8");

        iphoneCharger.chargeDevice(iphone, 3000);
        samsungCharger.chargeDevice(iphone, 2000);

        Charger.es.shutdown();
        System.out.println("done.");

    }



}
