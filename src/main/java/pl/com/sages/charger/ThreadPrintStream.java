package pl.com.sages.charger;

import java.io.PrintStream;

public class ThreadPrintStream extends PrintStream {


    public ThreadPrintStream() {
        super(System.out);
    }

    @Override
    public void println(String x) {
        super.println(Thread.currentThread().getName() + " : " + x);
    }

    @Override
    public void println(Object x) {
        println(x.toString());
    }
}
