package pl.com.sages.charger;

import java.util.concurrent.atomic.AtomicInteger;

public class Phone {

    private AtomicInteger battery;

    private String name;

    public Phone(int battery, String name) {
        this.battery = new AtomicInteger(battery);
        this.name = name;
    }

    public /*synchronized*/ void charge(int circles) {

        for (int i = 1; i <= circles; i++) {

           battery.incrementAndGet();
           //synchronized ( this ) {
            //   battery++; // battery = battery + 1
           //}
        }
        System.out.println("charging finished: " + this);
    }

    @Override
    public String toString() {
        return "Phone{" +
                "battery=" + battery +
                ", name='" + name + '\'' +
                '}';
    }
}
