package pl.com.sages.alarm;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Alarm {

    public static void main(String[] args) {

        ExecutorService es = Executors.newFixedThreadPool(2);

        Beeper b = new Beeper();
        Light l = new Light();

       /* Thread t1 = new Thread(b);
        Thread t2 = new Thread(l);
        t1.start();
        t2.start();*/

        es.execute(b);
        es.execute(l);

        es.shutdown();

        System.out.println("done.");



    }
}
