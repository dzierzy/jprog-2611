package pl.com.sages.net;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Client {

    public static void main(String[] args) {
        System.out.println("Client.main");

        ExecutorService es = Executors.newFixedThreadPool(2);

        try {
            Socket socket = new Socket("localhost", 777);
            System.out.println("connected to server.");

            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream());

            MessageReader mr = new MessageReader(reader);
            MessageWriter mw = new MessageWriter(writer);

            es.execute(mr);
            es.execute(mw);

            es.shutdown();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
