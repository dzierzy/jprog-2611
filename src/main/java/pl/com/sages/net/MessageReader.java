package pl.com.sages.net;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Created by marcin on 12.10.2016.
 */
public class MessageReader implements Runnable {

    private BufferedReader reader;

    public MessageReader(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public void run() {
        System.out.println("reader started.");
        String received = null;
        try(BufferedReader myReader = reader) {
            while((received = myReader.readLine())!=null && !"quit".equals(received)){
                System.out.println("< " + received + " >");
            }
        } catch (IOException e) {
        }
    }
}
