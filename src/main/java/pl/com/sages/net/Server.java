package pl.com.sages.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    public static void main(String[] args) {
        System.out.println("Server.main");

        ExecutorService es = Executors.newFixedThreadPool(20);

        try {

            ServerSocket server = new ServerSocket( 777 );

            System.out.println("waiting for a client, listening on a port 777 ...");
            while(true) {
                Socket socket = server.accept();
                System.out.println("client connected: " + socket.getInetAddress());

                Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        try {
                            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                            PrintWriter writer = new PrintWriter(socket.getOutputStream());

                            Runnable mr = new MessageReader(reader);
                            Runnable mw = new MessageWriter(writer);

                            es.execute(mr);
                            es.execute(mw);
                        }catch (IOException ioe){
                            throw new RuntimeException(ioe);
                        }
                    }
                };
                es.execute(r);


            }

            //es.shutdown();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
