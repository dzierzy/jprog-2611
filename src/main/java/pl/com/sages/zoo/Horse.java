package pl.com.sages.zoo;

public class Horse extends Animal  {


    public Horse(String name) {
        this.name = name;
    }


    @Override
    public void move() {
        System.out.println("horse is running !!!");
    }

    @Override
    public int getSize() {
        return 300;
    }


}
