package pl.com.sages.zoo;

public abstract class Animal implements Comparable<Animal>{

    protected String name;

    public String eat(String food){
        System.out.println("eating " + food + "...");
        return "^#&*^*%%^";
    }

    public abstract void move();

    public abstract int getSize();


    @Override
    public int compareTo(Animal o) {
        return name.compareTo(o.name);
    }

    @Override
    public boolean equals(Object o) {
        System.out.println("equals for name " + name + " in " + getClass().getName());
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Animal animal = (Animal) o;

        return name != null ? name.equals(animal.name) : animal.name == null;
    }

    @Override
    public int hashCode() {
        System.out.println("hashCode for " + name);
        return name != null ? name.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                "size='" + getSize() + '\'' +
                '}';
    }
}
