package pl.com.sages.zoo;

import java.util.*;

public class ZooStarter {

    public static void main(String[] args) {
        System.out.println("ZooStarter.main");

        Animal a = getAnimal();

        String result = a.eat("banan");
        System.out.println("->" + result);

        a.move();

        Box<Animal> box = new Box<>();
        box.put(a);
        Animal o = box.get();

        Set<Box<Animal>> animalBoxSet = new HashSet<>();

        Comparator<Animal> comparator =
                //new AnimalComparator();
                /*new Comparator<Animal>() {
                    @Override
                    public int compare(Animal o1, Animal o2) {
                        return o1.name.compareTo(o2.name);
                    }
                };*/
                (a1, a2) ->  a1.name.compareTo(a2.name);

        //List<Animal> residents = new ArrayList<>();
        Set<Animal> residents = new TreeSet<>((a1, a2) -> a1.name.compareTo(a2.name));

        Animal janusz = new Grizzly("Janusz");
        Animal zoe = new Shark("Zoe");
        residents.add(new Horse("Krzysztof"));
        residents.add(new Shark("Jack"));
        residents.add(janusz);
        residents.add(zoe);
        residents.add(new Eagle("Zawisza"));
        residents.add(janusz);

        zoe.name = "Poe";

        Set<Animal> tmp = new TreeSet<>();
        tmp.addAll(residents);

        residents = tmp;

        //Animal first = residents.get(0);

        System.out.println("there is " + residents.size() + " residents in our zoo");

        System.out.println("before sort: " + residents);
        // residents.sort(new AnimalComparator(true));
        // Collections.sort(residents, new AnimalComparator());

        for( Animal animal : residents ){
            System.out.println(animal);
            /*if(animal.name.equals("Jack")){
                residents.remove(animal);
            }*/
        }

        Iterator<Animal> itr = residents.iterator();
        while(itr.hasNext()){
            Animal animal = itr.next();
            if(animal.name.equals("Jack")){
                itr.remove();
            }
        }

        System.out.println("after sort: " + residents);



        Map<Animal, Integer> consumptionMap = new HashMap<>();

        Horse horse = new Horse("horse");
        //consumptionMap.put(new Horse("horse"), 15);
        consumptionMap.put(new Grizzly("grizzly"), 10);
        consumptionMap.put(new Eagle("eagle"), 3);
        consumptionMap.put(horse, 2);
        consumptionMap.put(new Horse("horse2"), 3);

        horse.name = "horse3";

        // TODO equals & hashCode
        Integer horseConsumption = consumptionMap.get(new Horse("horse3"));
        System.out.println("horse is consuming " + horseConsumption + " kilos daily");


    }

    private static Animal getAnimal(){
        return new Horse("Heniek");
    }

}
