package pl.com.sages.zoo;

public final class Grizzly extends Bear {


    public Grizzly(String name) {
        this.name = name;
    }

    @Override
    public void move() {
        System.out.println("grizzly is running");
    }

    @Override
    public int getSize() {
        return 600;
    }

    @Override
    public void findHoney() {

    }


}
