package pl.com.sages.zoo;

public class Box<T> {

    private T object;

    public void put(T object){
        this.object = object;
    }

    public T get(){
        return object;
    }
}
