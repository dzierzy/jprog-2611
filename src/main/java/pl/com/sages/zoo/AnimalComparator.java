package pl.com.sages.zoo;

import java.util.Comparator;

public class AnimalComparator implements Comparator<Animal> {

    private boolean asc = true;

    public AnimalComparator(boolean asc) {
        this.asc = asc;
    }

    public AnimalComparator(){
        this(true);
    }

    @Override
    public int compare(Animal o1, Animal o2) {
        //return asc ? o1.getSize() - o2.getSize() : o2.getSize() - o1.getSize();
        return o1.name.compareTo(o2.name);
    }
}
