package pl.com.sages.zoo;

public class Shark extends Animal {

    public Shark(String name) {
        this.name = name;
    }

    @Override
    public void move() {
        System.out.println("just swimming...");
    }

    @Override
    public int getSize() {
        return 200;
    }
}
