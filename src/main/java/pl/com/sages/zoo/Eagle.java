package pl.com.sages.zoo;

public class Eagle extends Animal {


    public Eagle(String name) {
        this.name = name;
    }

    @Override
    public void move() {
        System.out.println("just flying...");
    }

    @Override
    public int getSize() {
        return 40;
    }
}
