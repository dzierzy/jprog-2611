package pl.com.sages.javafx.guess;

import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class GuessNumberApp extends Application {

    private SecretNumber secret = new SecretNumber();

    private TextField guessField;

    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("Guess My Number");

        GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(25, 25, 25, 25));
        ColumnConstraints column1 = new ColumnConstraints();
        column1.setHalignment(HPos.CENTER);
        pane.getColumnConstraints().add(column1);

        Scene scene = new Scene(pane, 400, 200);

        pane.add(new Text("There is a number to guess! It's bettwen 1 and 100."), 0, 0);

        Label guessLabel = new Label("Your guess: ");
        pane.add(guessLabel, 0, 1);

        guessField = new TextField();
        pane.add(guessField, 0, 2);

        // TODO on key pressed

        guessField.setOnKeyPressed( e -> {
            if(e.getCode().equals(KeyCode.ENTER)){
                handle(guessField.getText());
            }
        });


        Button button = new Button("Check !");
        pane.add(button, 0, 3);

        // TODO set button on action
        button.setOnAction( e -> handle(guessField.getText()));

        primaryStage.setScene(scene);
        primaryStage.show();
    }


    private void handle(String value){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        // TODO set title
        // TODO set header
        // TODO verify number and prepare message
        // TODO set content text
        alert.setTitle("Result");
        alert.setHeaderText("The result is ....");

        int guessNumber = Integer.parseInt(value);
        int result = secret.guess(guessNumber);

        String message = result==0 ? "... correct!" : result<0 ? "... too big :(" : "... too little";
        alert.setContentText(message);

        alert.showAndWait();
    }

}
