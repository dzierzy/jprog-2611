package pl.com.sages.vehicle;

@FunctionalInterface
public interface Transportation {

    /*public abstract */
    void transport(String passenger);

    default String getTranporterName(){
        return "unknown";
    }

}
