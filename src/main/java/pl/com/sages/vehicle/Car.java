package pl.com.sages.vehicle;

public class Car /* extends Machine */ implements Transportation {

    public void drive(){
        System.out.println("driving !!!!");
    }

    @Override
    public void transport(String passenger) {
        System.out.println("loading passenger " + passenger);
        drive();
    }

    @Override
    public String getTranporterName() {
        return "";
    }
}
