package pl.com.sages.vehicle;

public class VehicleStarter {

    public static void main(String[] args) {
        System.out.println("VehicleStarter.main");

        Transportation t = p -> {
            System.out.println("adhoc transporter is transporting passenger " + p);
            System.out.println("adhoc transporter is transporting passenger " + p);
        };


        /*        new Transportation() {
            @Override
            public void transport(String passenger) {
                System.out.println("anonymous tranporter is transporting passenger " + passenger);
            }
        };*/


        t.transport("Jan Kowalski");


    }

}
