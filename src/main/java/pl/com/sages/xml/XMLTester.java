package pl.com.sages.xml;


import pl.com.sages.xml.dom.DOMTrainingParser;
import pl.com.sages.xml.sax.SaxTrainingParser;
import pl.com.sages.xml.xpath.XPathStudents;
import pl.com.sages.xml.xsl.XSLStudentsTransform;

public class XMLTester {
    public static void main(String[] args) throws Exception {

        System.out.println("<< DOM >>");
        new DOMTrainingParser().parseStudents();

        System.out.println("<< SAX >>");
        new SaxTrainingParser().parseStudents();

        System.out.println("<< XPATH >>");
        new XPathStudents().parseStudents();

        System.out.println("<< XSLT >>");
        new XSLStudentsTransform().transformStudents();
    }
}
